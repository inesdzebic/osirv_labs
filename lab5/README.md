# Lab 5 - Plotting, histogram, noise, median filtering and thresholding

#### Inspiracija

<small>
Ovaj dio vježbe uvelike se bazira na dva online Matplotlib tutoriala, te za
više informacija pogledajte slijedeće linkove: 
</small>

- [loria.fr Matplotlib tutorial](http://www.loria.fr/~rougier/teaching/matplotlib/)
- [Scipy lecture notes Matplotlib tutorial](https://scipy-lectures.github.io/intro/matplotlib/matplotlib.html#contour-plots)

Također odličan tutorial nalazi se na službenim matplotlib stranicama: 
[[pyplot_tutorial]](http://matplotlib.org/users/pyplot_tutorial.html)

## Zadaci

Potrebno je kreirati direktorij ` report ` i u njemu file ` README.md `. Unutar
tog filea je potrebno sastaviti report s rješenjima zadataka iz vježbe.

## Uvod u Matplotlib

Matplotlib je vjerojatno najčešće korišten Python paket za vizualizaciju 2D
grafike. Ovaj paket omogućuje i brz način vizualizacije podataka iz Pythona, a
isto tako i kreiranje kvalitetnih slika u velikom broju različitih formata.  

` matplotlib.pyplot ` je kolekcija funkcija koja omogućuje matplotlibu
iscrtavanje slično kao u MATLAB-u. Tako da će oni koji su imali do sada
iskustva s grafovima u MATLAB-u se moći vrlo brzo priviknuti. 

` matplotlib.pyplot ` je _stateful_, što znači da pamti stanje trenutnog grafa,
te su sve naredbe usmjerene prema trenutnom grafu. 



### Jednostavni plotovi

U ovom dijelu ćemo pokazati kako pomoću matplotliba crtati neke
jednostavne grafove. Počet ćemo s defaultnim postavkama, te ćemo polako
uljepšavati grafove. 

Kao prvi korak moramo stvoriti podatke koje ćemo prikazati na plotovima.

```python
import numpy as np

X    = np.linspace(-np.pi, np.pi, 256, endpoint=True)
C, S = np.cos(X), np.sin(X)
```

Gornji kod će stvoriti numpy polje X koje će imati 256 elemenata u rasponu od
$` -\pi `$ do $` \pi `$ (`linspace` smo spominjali u prošloj vježbi).
Nakon toga u varijable C i S spremamo kosinuse i sinuse svakog elementa polja
X. Tako sada C i S polja imaju jednak broj elemenata kao i polje X, što vidimo
iz slijedećeg koda.

```bash
>> print X.shape, X.min(), X.max()
(256,) -3.14159265359 3.14159265359
>> print C.shape, C.min(), C.max()
(256,) -1.0 0.999924110115
>> print S.shape, S.min(), S.max()
(256,) -0.999981027349 0.999981027349
```

Kako bi mogli navedena polja plotati, moramo uključiti matplotlib paket u našu
skriptu. 

```
import matplotlib.pyplot as plt
```

Sada su plotting funkcije matplotliba uključene u našu skriptu pod ` plt `
imenom, te im pristupamo s ` plt. `. Osnovna funkcija za iscrtavanje (plottanje)
je ` plot() `, pozivamo ju s ` plt.plot( naziv_varijable ) `, a prima jedan ili
više parametara. Ukoliko predamo jedan parametar, tada funkcija pretpostavlja
da su sve vrijednosti unutar predane varijable vrijednosti koje treba iscrtati
na $`  y  `$ os, dok na $`  x  `$ os postavlja vrijednosti od 0 do broja
elemenata u predanoj varijabli. Funkcija ` plt.show() ` prikazuje plot
kreiran ` plot() ` funkcijom.

**Isprobajte slijedeći kod:**

```python
import numpy as np
import matplotlib.pyplot as plt

X    = np.linspace(-np.pi, np.pi, 256, endpoint=True)
C, S = np.cos(X), np.sin(X)

plt.plot( X )
plt.show()
plt.plot( C )
plt.show()
plt.plot( S )
plt.show()
```

Ukoliko želimo plotati dvije varijable gdje je jedna ovisna o drugoj, kao u
ovom slučaju, gdje je ` X ` nezavisna varijabla a ` C ` i ` S ` su zavisne
varijable, predat ćemo ih funkciji obje kao parametre. Prvo predajemo nezavisnu
varijablu, u ovom slučaju ` X `, te ona ide na $`  x  `$ os, a nakon toga
predajemo zavisnu varijablu ( u ovom slučaju ` C ` i ` S ` ) koja ide na 
$`  y  `$ os.

**Isprobajte slijedeći kod:**

```python
import numpy as np
import matplotlib.pyplot as plt

X    = np.linspace(-np.pi, np.pi, 256, endpoint=True)
C, S = np.cos(X), np.sin(X)

plt.plot( X, C )
plt.show()
plt.plot( X, S )
plt.show()
```

Ukoliko želimo plotati više od jedne varijable na istom grafu, tada ih dodajemo
s ` plot() ` funkcijom, bez poziva ` plt.show() ` između. Ovaj način je
ekvivalentan ` hold on ` naredbi u MATLAB-u. Poziv funkcije ` plt.show() `
iscrtava trenutni graf na ekran, te se svi novi pozivi ` plt.plot() ` funkcije
izvršavaju na novom grafu, koji se isto prikazuje ponovnim pozivom ` plt.show() ` 
funkcije.

**Isprobajte slijedeći kod:**

```python
import numpy as np
import matplotlib.pyplot as plt

X    = np.linspace(-np.pi, np.pi, 256, endpoint=True)
C, S = np.cos(X), np.sin(X)

plt.plot( X, C )
plt.plot( X, S )
plt.show()
```

Moguće je, koristeći sličnu sintaksu kao u MATLAB-u, dodavati nazive na osi,
naslov grafa, mijenjati boje, debljine i oblike linija i slično. Također je
moguće renderirati matematičke formule direktno unutar teksta na grafu, za što
se koristi LaTeX sintaksa. Pogledajte slijedeći primjer.


**Isprobajte slijedeći kod:**

```python
import numpy as np
import cv2
import matplotlib.pyplot as plt

X    = np.linspace(-np.pi, np.pi, 256, endpoint=True)
C, S = np.cos(X), np.sin(X)
R    = np.random.uniform(-1,1,256) #uniformni šum od -1 do 1

plt.plot( X, R, 'm-', label="uniform noise") #boja magenta, linija
plt.plot( X, C, 'ro', label="cosine")        #boja red, krugovi
plt.plot( X, S, 'g.', label="sine" )         #boja green, točkice
plt.xlabel(r"$x$ os - raspon od $-\pi$ do $\pi$ ") 
plt.ylabel(r"Vrijednost amplitude sinusa i kosinusa ") 
plt.title(r"Hello pyplot - $\sqrt{x^2 + y^2}$!")
plt.legend(loc="upper left")
plt.grid()
plt.show()
```

U gornjem primjeru možemo primijetiti ` r"slovo 'r' ispred stringova" ` u
funkcijama ` xlabel `, ` ylabel ` i  ` title `. Razlog su znakovi ` \ ` 
unutar stringa. slovo ` r ` prije stringa označava da se radi o _ raw _
stringu, te neće _escapeati_ znakove unutar tog stringa. Ti su nam znakovi
potrebni jer matplotlib ima u sebi LaTeX prevoditelj koji prevodi LaTeX
sintaksu u matematičke formule.

#### Vertikalne linije

Čest način prikaza podataka u grafovima osim krivulje je i koristeći vertikalne
linije. Često ga koristimo za kvantitativno prikazivanje podataka po
kategorijama ( koliko ima pripadnika koje kategorije ). Primjer koji se često
koristi je prikaz uspješnosti na ispitima. U tom slučaju imamo 5 vertikalnih
linija koje predstavljaju 5 ocjena, a visina svake vertikalne linije (ili
stupca) predstavlja koliko je ocjena u toj kategoriji. Možemo pogledati
slijedeći primjer.


**Isprobajte slijedeći kod:**

```python
import numpy as np
import matplotlib.pyplot as plt

broj_ocjena = [ 7, 12, 18, 9, 5 ]
ocjene = [ 1, 2, 3, 4, 5 ]

plt.plot(ocjene, broj_ocjena)
plt.show()
```

Iako i ovaj graf prikazuje iste podatke, način na koji ih prikazuje ne odgovara
namjeni. Nama je cilj iz ovoga vidjeti koliko ima određenih ocjena, koje su
diskretni događaji, a to iz ovog grafa nije lako vidljivo. 

Bolji način prikaza ovakvih podataka je kao na idućem primjeru.

**Isprobajte slijedeći kod:**

```python
import numpy as np
import matplotlib.pyplot as plt

broj_ocjena = [ 7, 12, 18, 9, 5 ]
ocjene = [ 1, 2, 3, 4, 5 ]

plt.bar(ocjene, broj_ocjena, color='magenta') #moguće je mijenjati boju stupaca
plt.show()
```

Iz ovog grafa je odmah jasno što su to ocjene i koliko kategorija ocjena ima,
te kojih ocjena ima najviše. Ipak, što ako imamo puno kategorija koje trebamo
prikazati. Primjer je ako želimo prikazati promjenu temperature tijekom dijela
godine,
te imamo 150 dana za koje želimo prikazati temperaturu za svaki pojedini
dan. 


**Isprobajte slijedeći kod:**

```python
import numpy as np
import matplotlib.pyplot as plt

temps = np.random.poisson(20,150) #uzima 150 random vrijednosti iz poissonove distribucije 

plt.bar(np.arange(len(temps)), temps)
plt.show()
```

U ovakvom slučaju graf ne izgleda baš najlijepše, jer matplotlib pokušava oko
svakog stupca ocrtati crnu liniju, a stupac obojati u neku boju (default
plavu).
Za slučaje kada imamo jako puno kategorija koje želimo prikazati, bolji način
je umjesto stupca iscrtavati samo vertikalnu liniju. To možemo korištenjem
funkcije ` plt.vlines() `.


**Isprobajte slijedeći kod:**

```python
import numpy as np
import matplotlib.pyplot as plt

temps = np.random.poisson(20,150)

plt.vlines(np.arange(len(temps)), 0 , temps)
plt.show()
```

U prijašnjem primjeru ` plt.bar() ` funkcija je primala dva parametra, polja
vrijednosti na $`  x  `$ i $`  y  `$ osi. Funkcija ` plt.vlines() ` prima tri
parametra, gdje je prvi kao i kod ` plt.bar() ` polje vrijednosti na $`  x  `$
osi, dok je ovdje drugi parametar minimalna vrijednost linije na $`  y  `$ osi,
a treći vrijednosti na $`  y  `$ osi do kojih treba iscrtati linije.

Isto kao i u dosadašnjim primjerima, i vertikalne linije možemo uređivati,
mijenjati im boju, dodavati tekst, te plotati više različitih tipova plotova na
isti graf.


**Isprobajte slijedeći kod:**

```python
import numpy as np
import matplotlib.pyplot as plt

temps = np.random.poisson(20,150)
temps3 = temps.copy()
temps2 = np.zeros_like(temps)
temps2[70:] = temps[70:].copy()
temps[70:] = 0

plt.plot(np.arange(1,len(temps3)+1), temps3, label="maximum", color="red")
plt.vlines(np.arange(1,len(temps)+1),0, temps, label="sijecanj", color="magenta")
plt.vlines(np.arange(1,len(temps2)+1),0, temps2, label="veljaca", color="green")
plt.legend(loc="upper left")
plt.show()
```

## Histogram


- https://en.wikipedia.org/wiki/Histogram
- https://en.wikipedia.org/wiki/Image_histogram

##### Implementacija histograma:

Pretpostavimo da se slika za koju želimo histogram nalazi u varijabli imena `
image `, te da je tip slike _uint8 grayscale_.

```python
hist = np.zeros(256)

for i in image:
    for j in i:
        hist[j] += 1
```

Taj histogram tada možemo prikazati slijedećim kodom:

```python
plt.vlines(np.arange(len(hist)), 0 , hist)
plt.show()
```

Numpy također ima ugrađenu funkciju za računanje histograma. Tu je funkciju
preporučljivo koristiti jer je puno brža od python verzije, a i nije definirana
samo za grayscale slike od 256 razina sive, već se može koristiti za bilo kakve
slike. 

```python
hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
```

Ova funkcija vraća dva polja, jedno u kojem je histogram i drugo u kojem se
_binovi_. Prima kao parametre 1D polje za koje treba napraviti histogram (kako
je slika 2D polje potrebno ju je _sravnati_ na 1D korištenjem ` .flatten() `
funkcije), broj _binova_, te raspon vrijednosti u histogramu. Ako se ne preda
kao parametar broj binova, koristit će se defaultna vrijednost, koja je 10. Bez
` range ` parametra  se može pozivati funkcija.


## Noise

Wikipedia: [Image noise]( https://en.wikipedia.org/wiki/Image_noise )

**Za prikazivanje utjecaja šuma na sliku koristit ćemo slijedeću sliku. Slika
se nalazi u direktoriju ` images ` u rootu projekta pod imenom `noise_template.png`**

![Slika za prikazivanje utjecaja šuma]( ../images/noise_template.png )

### Gaussian noise

Gaussov šum predstavlja statistički šum u kojem je vjerojatnost pojavljivanja
određene vrijednosti jednaka normalnoj tj. Gaussovoj distribuciji. 

Vjerojatnost pojavljivanja neke slučajne vrijednosti $`  z  `$ dana je s:

![gauss formula](https://upload.wikimedia.org/math/c/7/0/c70012e2b38059f77ba8b6bb4cea7e2c.png)

gdje je $`  z  `$ razina sive boje, $`  \mu  `$ je srednja vrijednost, dok je
$`  \sigma  `$ standardna devijacija.

Gaussova distribucija pri određenim parametrima $`  \mu  `$ i $`  \sigma  `$
je:

![gauss distrib](https://upload.wikimedia.org/wikipedia/commons/thumb/7/74/Normal_Distribution_PDF.svg/720px-Normal_Distribution_PDF.svg.png)

Više o Gaussovom šumu: 

- [Gaussova distribucija](https://en.wikipedia.org/wiki/Gaussian_distribution)
- [Gaussov šum](https://en.wikipedia.org/wiki/Gaussian_noise)

**Izgled gaussove distribucije možemo dobiti slijedećim kodom:**

```python
import numpy as np
import matplotlib.pyplot as plt

mu = 0
sigma = 0.1
gauss = np.random.normal(mu, sigma, 100000)
hist,bins = np.histogram(gauss, bins=256 )
plt.vlines(bins[1:], 0, hist)
plt.show()
```

### Uniform noise


Uniformni šum se još naziva i kvantizacijski šum. Nastaje najčešće pri
kvantiziranju piksela ulazne slike na određeni broj dikretnih razina. Ima
otprilike uniformnu razdiobu. To znači da svaka vrijednost unutar nekog raspona
ima jednaku vjerojatnost pojavljivanja.

![uniform_formula](https://upload.wikimedia.org/math/8/f/b/8fbfebfbb3dfa135da807a45374376d5.png)

gdje su $`  a  `$ i $`  b  `$ granice unutar kojih se vrijednost može pojaviti.

Ovako izgleda uniformna razdioba:

![uniform_dist](https://upload.wikimedia.org/wikipedia/commons/9/96/Uniform_Distribution_PDF_SVG.svg)

Više:

- [ Quantization (Uniform) noise ](https://en.wikipedia.org/wiki/Image_noise#Quantization_noise_.28uniform_noise.29) 
- [ Uniform distribution ]( https://en.wikipedia.org/wiki/Uniform_distribution_%28continuous%29)

**Izgled uniformne distribucije možemo dobiti slijedećim kodom:**

```python
import numpy as np
import matplotlib.pyplot as plt

gauss = np.random.uniform(-30, 30, 100000)
hist,bins = np.histogram(gauss, bins=256 )
plt.vlines(bins[1:], 0, hist)
plt.xlim(-100,100)
plt.show()
```


### Salt and pepper noise

Salt and pepper šum je oblik šuma gdje je neki postotak slučajnih  piksela na slici ili
bijel ili  crn.

![snp](https://upload.wikimedia.org/wikipedia/commons/thumb/f/f4/Noise_salt_and_pepper.png/220px-Noise_salt_and_pepper.png)


## Implementacija šuma 

Slijedeći kod sadrži implementaciju tri gore opisana tipa šuma. Također je
prikazano i na primjeru slike kako koji od šumova utječe na sliku.

```
import numpy as np
import cv2
import matplotlib.pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

def salt_n_pepper_noise(img, percent=10):
  out = img.astype(np.float32)
  limit = ((float(percent)/2.0)/100.0) * 255.0
  noise = np.random.uniform(0,255, img.shape)
  out[noise<limit] = 0
  out[noise>(255-limit)] = 255
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)

def uniform_noise(img, low, high):
  out = img.astype(np.float32)
  noise = np.random.uniform(low,high, img.shape)
  out = out + noise
  out[out>255] = 255
  out[out<0] = 0
  return out.astype(np.uint8)


def show(img):
  cv2.imshow("title", img)
  cv2.waitKey(0)
  cv2.destroyAllWindows()
def showhist(img):
  hist,bins = np.histogram(img.flatten(), bins=256, range=(0,255))
  plt.vlines(np.arange(len(hist)), 0, hist)
  plt.title("Histogram")
  plt.show()

img = cv2.imread("../noise_template.png",0)


show(img)
showhist(img)

show(salt_n_pepper_noise(img,20))
showhist(salt_n_pepper_noise(img, 20))

show(gaussian_noise(img, 0, 12))
showhist(gaussian_noise(img, 0, 12))

show(uniform_noise(img, -30, 30))
showhist(uniform_noise(img, -30, 30))
```



### Zadaci:

1. Na sliku po izboru dodajte sva tri tipa šuma s parametrima:

  - Gaussian: $`  \mu = 0  `$, $`  \sigma = \[ 1, 5, 10, 20, 40, 60 \]  `$
  - Uniform: granice $`  = [(-20,20), (-40,40), (-60,60)]  `$
  - Salt and pepper: postotak slike $`  =  [ 5\%, 10\%, 15\%, 20% ]  `$
  
  **Snimite stvorene slike.** Prikažite histograme za svaku od slika. Probajte
  uočiti utjecaj šuma u histogramu slike.


# Median filtering

### Brief Description

The median filter is normally used to reduce noise
in an image, somewhat like the mean  (averaging) filter. However, it often does a
better job than the mean filter of preserving useful detail in the
image.


### How It Works

Like the mean (averaging) filter, the median filter considers each pixel in the
image in turn and looks at its nearby neighbors to decide whether or
not it is representative of its surroundings. Instead of simply
replacing the pixel value with the <EM>mean</EM> of neighboring pixel
values, it replaces it with the <EM>median</EM> of those values. The
median is calculated by first sorting all the pixel values from the
surrounding neighborhood into numerical order and then replacing the
pixel being considered with the middle pixel value.  (If the
neighborhood under consideration contains an even number of pixels,
the average of the two middle pixel values is used.) Following image 
illustrates an example calculation.

<CENTER><IMG ALT="" SRC="http://homepages.inf.ed.ac.uk/rbf/HIPR2/figs/med3x3.gif"></CENTER>

 Calculating the median value of a pixel neighborhood. As
can be seen, the central pixel value of 150 is rather unrepresentative
of the surrounding pixels and is replaced with the median value:
124. A 3&#215;3 square neighborhood is used here --- larger
neighborhoods will produce more severe smoothing.

### Guidelines for Use

<P>By calculating the median value of a neighborhood rather than the
mean value, the median filter has two main advantages over
the mean filter:

- The median is a more robust average than the mean and so a
single very unrepresentative pixel in a neighborhood will not affect
the median value significantly.

- Since the median value must actually be the value of one of the
pixels in the neighborhood, the median filter does not create new
unrealistic pixel values when the filter straddles an edge. For this
reason the median filter is much better at preserving sharp edges than
the mean filter.

You can use median filter with the following code:

```
median = cv2.medianBlur( image, radius )
```

where image is numpy array containing the image, and radius is an integer which
defines the radius of the neighborhood for filtering.

You can use gaussian blur filter with the following code:

```
blur = cv2.GaussianBlur( image, (kernelXsize, kernelYsize), sigma )
```

where image is numpy array containing the image, (kernelXsize, kernelYsize) is
a tuple containing the size of the kernel ( e.g. `(5, 5)` for 5x5 kernel ) and
sigma is the value of $`  \sigma  `$ parameter.


## Exercises:

1. Corrupt images `boats` and `airplane` with salt and pepper noise with 
$`  percent = [ 1\%, 10\% ]  `$, and gaussian noise with $`  \sigma = [
5, 15, 35]  `$. 

2. Using the corrupted images explore  the effect of median filtering with
different neighborhood sizes. 

2. Filter the corrupted images with median filter and gaussian blur filter. 
Try to determine which filter is better for removal of which type of noise.
Use various parameters for filters.

2. Implement the median filter yourself. Median filter
function should take two parameters: image to be filtered and radius of median
filter. Filtering kernel should be a square. Test your implementation against
openCV implementation. Double points for using C code in Weave (similar to
convolution example).

First three exercises are graded 1 point together, exercise 4 is graded 1 point
for python implementation, 2 points for Weave.


#  Thresholding

### What is Thresholding?

The simplest segmentation method

Application example: Separate out regions of an image corresponding to
objects which we want to analyze. This separation is based on the variation
of intensity between the object pixels and the background pixels.

To differentiate the pixels we are interested in from the rest (which will
eventually be rejected), we perform a comparison of each pixel intensity
value with respect to a threshold (determined according to the problem to
solve).

Once we have separated properly the important pixels, we can set them with
a determined value to identify them (i.e. we can assign them a value of 0
(black), 255 (white) or any value that suits your needs).

![](http://docs.opencv.org/_images/Threshold_Tutorial_Theory_Example.jpg)


### Simple Thresholding

Here, the matter is straight forward. If pixel value is greater than a
threshold value, it is assigned one value (may be white), else it is assigned
another value (may be black). The function used is `cv2.threshold`. First
argument is the source image, which should be a grayscale image. Second
argument is the threshold value which is used to classify the pixel values.
Third argument is the ` maxVal ` which represents the value to be given if pixel
value is more than (sometimes less than) the threshold value. OpenCV provides
different styles of thresholding and it is decided by the fourth parameter of
the function. Different types are:

- cv2.THRESH_BINARY
- cv2.THRESH_BINARY_INV
- cv2.THRESH_TRUNC
- cv2.THRESH_TOZERO
- cv2.THRESH_TOZERO_INV

To illustrate how these thresholding processes work, let’s consider that we
have a source image with pixels with intensity values $` src(x,y) `$. 
The plot below
depicts this. The horizontal blue line represents the threshold $` thresh `$ (fixed).

![](http://docs.opencv.org/_images/Threshold_Tutorial_Theory_Base_Figure.png)

Documentation clearly explain what each type is meant for. [Please check out the
documentation](http://docs.opencv.org/doc/tutorials/imgproc/threshold/threshold.html).


Using the `cv2.threshold()` function
two outputs are obtained. First one is a retval which will be explained later.
Second output is our thresholded image.

For this example download following image and save it to
**`slike/gradient.png`**

![](https://dl.dropboxusercontent.com/u/16407123/mosirv%20slike/gradient.png)

```
import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('gradient.png',0)
ret,thresh1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
ret,thresh2 = cv2.threshold(img,127,255,cv2.THRESH_BINARY_INV)
ret,thresh3 = cv2.threshold(img,127,255,cv2.THRESH_TRUNC)
ret,thresh4 = cv2.threshold(img,127,255,cv2.THRESH_TOZERO)
ret,thresh5 = cv2.threshold(img,127,255,cv2.THRESH_TOZERO_INV)

titles = ['Original Image','BINARY','BINARY_INV','TRUNC','TOZERO','TOZERO_INV']
images = [img, thresh1, thresh2, thresh3, thresh4, thresh5]

for i in xrange(6):
    plt.subplot(2,3,i+1),plt.imshow(images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])

plt.show()

```


### Adaptive Thresholding

In the previous section, we used a global value as threshold value. But it may
not be good in all the conditions where image has different lighting conditions
in different areas. In that case, we go for adaptive thresholding. In this, the
algorithm calculate the threshold for a small regions of the image. So we get
different thresholds for different regions of the same image and it gives us
better results for images with varying illumination.

It has three ‘special’ input params and only one output argument.

Adaptive Method - It decides how thresholding value is calculated.

- `cv2.ADAPTIVE_THRESH_MEAN_C` : threshold value is the mean of
neighbourhood area.  
- `cv2.ADAPTIVE_THRESH_GAUSSIAN_C` : threshold value
is the weighted sum of neighbourhood values where weights are a
gaussian window.

Block Size - It decides the size of neighbourhood area.

C - It is just a constant which is subtracted from the mean or weighted mean calculated.

Below piece of code compares global thresholding and adaptive thresholding for
an image with varying illumination:

```
import cv2
import numpy as np
from matplotlib import pyplot as plt

img = cv2.imread('sudoku.jpg',0)
img = cv2.medianBlur(img,5)

ret,th1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)
th2 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_MEAN_C,\
            cv2.THRESH_BINARY,11,2)
th3 = cv2.adaptiveThreshold(img,255,cv2.ADAPTIVE_THRESH_GAUSSIAN_C,\
            cv2.THRESH_BINARY,11,2)

titles = ['Original Image', 'Global Thresholding (v = 127)',
            'Adaptive Mean Thresholding', 'Adaptive Gaussian Thresholding']
images = [img, th1, th2, th3]

for i in xrange(4):
    plt.subplot(2,2,i+1),plt.imshow(images[i],'gray')
    plt.title(titles[i])
    plt.xticks([]),plt.yticks([])
plt.show()
```

For this example the image located in 
**`images/sudoku.jpg`**
will be used. 

![](../images/sudoku.jpg)


### Otsu’s Binarization

In the first section, I told you there is a second parameter `retVal`. Its use
comes when we go for Otsu’s Binarization. So what is it?

In global thresholding, we used an arbitrary value for threshold value, right?
So, how can we know a value we selected is good or not? Answer is, trial and
error method. But consider a **bimodal image** (_In simple words, bimodal image is
an image whose histogram has two peaks_). For that image, we can approximately
take a value in the middle of those peaks as threshold value, right ? That is
what Otsu binarization does. So in simple words, it automatically calculates a
threshold value from image histogram for a bimodal image. (For images which are
not bimodal, binarization won’t be accurate.)

For this, our `cv2.threshold()` function is used, but pass an extra flag,
`cv2.THRESH_OTSU`. For threshold value, simply pass zero. Then the algorithm
finds the optimal threshold value and returns you as the second output,
`retVal`.
If Otsu thresholding is not used, `retVal` is same as the threshold value you
used.

Check out below example. Input image is a noisy image. In first case, I applied
global thresholding for a value of 127. In second case, I applied Otsu’s
thresholding directly. In third case, I filtered image with a 5x5 gaussian
kernel to remove the noise, then applied Otsu thresholding. See how noise
filtering improves the result.

For this example the image located in 
**`images/otsu.png`**
will be used. 

![]( ../images/otsu.png )


```
import cv2
import numpy as np
from matplotlib import pyplot as plt

def gaussian_noise(img, mu, sigma):
  out = img.astype(np.float32)
  noise = np.random.normal(mu, sigma, img.shape)
  out = out + noise
  out[out<0] = 0
  out[out>255] = 255
  return out.astype(np.uint8)

img = cv2.imread('otsu.png',0)
img = gaussian_noise(img, 0, 30)

# global thresholding
ret1,th1 = cv2.threshold(img,127,255,cv2.THRESH_BINARY)

# Otsu's thresholding
ret2,th2 = cv2.threshold(img,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

# Otsu's thresholding after Gaussian filtering
blur = cv2.GaussianBlur(img,(5,5),0)
ret3,th3 = cv2.threshold(blur,0,255,cv2.THRESH_BINARY+cv2.THRESH_OTSU)

# plot all the images and their histograms
images = [img, 0, th1,
          img, 0, th2,
          blur, 0, th3]
rets = [ret1, ret2, ret3]
titles = ['Original Noisy Image','Histogram','Global Thresholding (v=127)',
          'Original Noisy Image','Histogram',"Otsu's Thresholding",
          'Gaussian filtered Image','Histogram',"Otsu's Thresholding"]
print rets

for i in xrange(3):
    plt.subplot(3,3,i*3+1),plt.imshow(images[i*3],'gray')
    plt.title(titles[i*3]), plt.xticks([]), plt.yticks([])
    plt.subplot(3,3,i*3+2),plt.hist(images[i*3].ravel(),256)
    plt.title(titles[i*3+1]), plt.xticks([]), plt.yticks([])
    plt.plot([rets[i]]*1000, np.arange(0 ,1000,1), 'r')
    plt.annotate('threshold', xy=(rets[i], 1000),xytext=(rets[i] + 40, 1000), color="red", \
        arrowprops=dict(facecolor='red', shrink=0.05),)
    plt.subplot(3,3,i*3+3),plt.imshow(images[i*3+2],'gray')
    plt.title(titles[i*3+2]), plt.xticks([]), plt.yticks([])
plt.show()
```

In this example we first add gaussian noise with $` \sigma = 30  `$, to
disperse image grayscale values. 

## Exercise:

1. Compare the above thresholding methods on images `boats`, `baboon`
and `airplane`. Try using different thresholds for images.


