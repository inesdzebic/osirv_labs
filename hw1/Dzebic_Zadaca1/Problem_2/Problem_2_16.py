import numpy as np
import cv2

#koristi se isti kod kao u Problem_2_8, a broj n ovdje je podesen na zeljenu vrijednost - 16
def redak(ime_slike):
    slika = cv2.imread('/home/osirv/labs/Dzebic/osirv_labs/images/'+ime_slike)
    redak = slika.shape[0]
    n=16
    for x in range(0, redak):
        if (x%(2*n))==0:
            slika[x:x+n]=0 
    cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_2/Problem_2_16/"+ime_slike[0:-4]+"_H16.png", slika)        
    povratna=cv2.imshow(''+ime_slike[0:-4]+''+'_H', slika)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return povratna


def stupac (ime_slike):
    slika = cv2.imread('/home/osirv/labs/Dzebic/osirv_labs/images/'+ime_slike)
    stupac = slika.shape[1]
    n=16
    for y in range(0, stupac):
        if (y%(2*n))==0: 
            slika[:,y:y+n] = 0 
    cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_2/Problem_2_16/"+ime_slike[0:-4]+"_V16.png", slika)  
    povratna=cv2.imshow(''+ime_slike[0:-4]+''+'_V', slika)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return povratna

#pozivamo funkcije
redak('airplane.bmp')
stupac('airplane.bmp')

redak('asbest.pgm')
stupac('asbest.pgm')

redak('baboon.bmp')
stupac('baboon.bmp')

redak('barbara.bmp')
stupac('barbara.bmp')

redak('barbara2.pgm')
stupac('barbara2.pgm')

redak('boats.bmp')
stupac('boats.bmp')

redak('BoatsColor.bmp')
stupac('BoatsColor.bmp')

redak('goldhill.bmp')
stupac('goldhill.bmp')

redak('lenna.bmp')
stupac('lenna.bmp')

redak('pepper.bmp')
stupac('pepper.bmp')

redak('lenna2.png')
stupac('lenna2.png')
