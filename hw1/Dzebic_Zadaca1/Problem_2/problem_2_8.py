import numpy as np
import cv2

#koristi se isti kod kao za prethodni zadatak
def redak(ime_slike):
    slika = cv2.imread('/home/osirv/labs/Dzebic/osirv_labs/images/'+ime_slike)
    redak = slika.shape[0]
    n=8 #postavimo n - sirinu crnog retka
    for x in range(0, redak):
        if (x%(2*n))==0: 
            slika[x:x+n]=0 
    #prolazi se kroz retke i gleda se broj retka u odnosu na dvostruku vrijednost n-a (dvostruka vrijednost kako bi imali razmak izmedju crnih redaka)   
    
    cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_2/Problem_2_8/"+ime_slike[0:-4]+"_H8.png", slika)        
    povratna=cv2.imshow(''+ime_slike[0:-4]+''+'_H', slika)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return povratna

#identicno sve za stupce kao i za retke
def stupac (ime_slike):
    slika = cv2.imread('/home/osirv/labs/Dzebic/osirv_labs/images/'+ime_slike)
    stupac = slika.shape[1]
    n=8
    for y in range(0, stupac):
        if (y%(2*n))==0: 
            slika[:,y:y+n] = 0 
    cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_2/Problem_2_8/"+ime_slike[0:-4]+"_V8.png", slika)  
    povratna=cv2.imshow(''+ime_slike[0:-4]+''+'_V', slika)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return povratna

#pozivamo funkcije
redak('airplane.bmp')
stupac('airplane.bmp')

redak('asbest.pgm')
stupac('asbest.pgm')

redak('baboon.bmp')
stupac('baboon.bmp')

redak('barbara.bmp')
stupac('barbara.bmp')

redak('barbara2.pgm')
stupac('barbara2.pgm')

redak('boats.bmp')
stupac('boats.bmp')

redak('BoatsColor.bmp')
stupac('BoatsColor.bmp')

redak('goldhill.bmp')
stupac('goldhill.bmp')

redak('lenna.bmp')
stupac('lenna.bmp')

redak('pepper.bmp')
stupac('pepper.bmp')

redak('lenna2.png')
stupac('lenna2.png')
