# HOMEWORK1 - Author Ines Dzebic


## **Problem_2**


### **Task**

**Fat stripes**

The task was to create a program which will load all images from the images directory, to create images with stripes of widths 8, 16, 32, and to save the resulting images with the filename of type image_filename_VN and image_filename_HN, where: 
* V and H denotes vertical or horizontal stripes, while
* N denotes the stripe width.


### **Results**

Task was solved successfully. Since images, one by one, couldn't have been shown here, results can be found on the links below:

8pxl:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_2/Problem_2_8](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_2/Problem_2_8)

16pxl:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_2/Problem_2_16](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_2/Problem_2_16)

32pxl:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_2/Problem_2_32](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_2/Problem_2_32)


