# HOMEWORK1 - Author Ines Dzebic



## **Problem_5**


### **Task**

**Floyd-Steinberg Dithering**


The task was to improve the visual quality when quantising images using
Floyd-Steinberg Dithering, a special type of so-called error diffusion. It
distributes the quantisation error of a pixel to its unprocessed neighbouring
pixels as follows:

For each pixel (i,j), from left to right and top to bottom, task is to:


1. quantise $` u_{i,j} `$ with a method implemented in Lab 4
1. compute the error $` e `$ between the original value and the quantised
value
1. distribute the error $` e `$ to the neighbouring pixels that are not yet
visited as follows:

```math
u_{i+1,j} = u_{i+1,j} + (7/16)*e
```
```math
u_{i-1,j+1} = u_{i-1,j+1} + (3/16)*e
```
```math
u_{i,j+1} = u_{i,j+1} + (5/16)*e
```
```math
u_{i+1,j+1} = u_{i+1,j+1} + (1/16)*e
```
Quantisation values should be $`q \in [1, 2, 3] `$

Images should be saved with the filename format image_filename_qfsd.png,  where q is the
numeric value of q used for that image.



### **Results**



Task was solved successfully. Since images, one by one, couldn't have been shown here, results can be found on the link below:



Result for Floyd-Steinberg Dithering with q=1:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_5/Problem_5_1](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_5/Problem_5_1)

Result for Floyd-Steinberg Dithering with q=2:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_5/Problem_5_2](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_5/Problem_5_2)

Result for Floyd-Steinberg Dithering with q=3:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_5/Problem_5_3](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_5/Problem_5_3)






