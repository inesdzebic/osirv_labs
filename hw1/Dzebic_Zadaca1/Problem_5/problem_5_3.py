import numpy as np
import cv2
import math
import os

def fsd(ime_slike):
    slika = cv2.imread('/home/osirv/labs/Dzebic/osirv_labs/images/'+ime_slike, cv2.IMREAD_GRAYSCALE)
    q = 3 
    d = 2**(8-q)
    redak = slika.shape[0]
    stupac = slika.shape[1]
    for j in range(0, stupac):
        for i in range(0, redak):
            kvantizirana = slika.copy()
            slika = slika.astype(np.float32)
            if (kvantizirana [i][j]>255):
                kvantizirana[i][j]=255
            if (kvantizirana [i][j] <0):
                kvantizirana [i][j]=0
            kvantizirana [i][j]= (math.floor (slika[i][j]/d)+0.5)*d   
            
            oldpixel= slika[i][j]
            newpixel= kvantizirana[i][j]
            error=oldpixel-newpixel
            slika[i][j]= newpixel
            if(i< redak-1): 
                slika[i+1][j] = slika[i+1][j] + (7/16)*error
            if(i > 1 and j < stupac - 1):  
                slika[i-1][j+1] = slika[i-1][j+1] + (3/16)*error
            if(j< stupac-1): 
                slika[i][j+1] = slika[i][j+1] + (5/16)*error
            if(i < redak - 1 and j < stupac - 1):
                slika[i+1][j+1] = slika[i+1][j+1] + (1/16)*error
    slika =slika.astype(np.uint8 )

    cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_5/Problem_5_3/"+ime_slike[0:-4]+"_3fsd.png", slika)        
    povratna=cv2.imshow(''+ime_slike[0:-4]+''+'_fsd', slika)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return povratna

fsd('airplane.bmp')
fsd('asbest.pgm')
fsd('baboon.bmp')
fsd('barbara.bmp')
fsd('barbara2.pgm')
fsd('boats.bmp')
fsd('BoatsColor.bmp')
fsd('goldhill.bmp')
fsd('lenna.bmp')
fsd('pepper.bmp')
fsd('lenna2.png')