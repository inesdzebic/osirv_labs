import numpy as np
import cv2

def checkerboard(ime_slike):
    slika = cv2.imread('/home/osirv/labs/Dzebic/osirv_labs/images/'+ime_slike)
    redak = slika.shape[0]
    stupac = slika.shape[1]
    n=64 #definiramo sirinu kocke

    #prolazimo kroz dvije for petlje kroz piksele slike kao kroz matricu
    for x in range(0, redak):
        for y in range(0, stupac):
            #ako je i piksel stupca i piksel retka djeljiv s brojem 2*64 bez ostatka(dvostruki kako bismo imali jednak razmak)
            #zacrne se pikseli od tog piksela koji ispunjava uvjet za n=64 po retku i po stupcu
            if (y%(2*n))==0 and (x%(2*n))==0: 
                slika[x:x+n,y:y+n] = abs(255-slika[x:x+n,y:y+n]) #255-slika kako bismo dobili invert piksela
            #ponovno isti uvjet ali piksel pomaknut za n=64 mjesta i u retku i u stupcu kako bismo dobili sahovsku plocu    
            if ((y+n)%(2*n))==0 and ((x+n)%(2*n))==0:
                slika[x:x+n,y:y+n] = abs(255-slika[x:x+n,y:y+n]) #255-slika kako bismo dobili invert piksela
    cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_4/Problem_4/"+ime_slike[0:-4]+"_checkerboard.png", slika)        
    povratna=cv2.imshow(''+ime_slike[0:-4]+''+'_checkerboard', slika)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return povratna

checkerboard('airplane.bmp')
checkerboard('asbest.pgm')
checkerboard('baboon.bmp')
checkerboard('barbara.bmp')
checkerboard('barbara2.pgm')
checkerboard('boats.bmp')
checkerboard('BoatsColor.bmp')
checkerboard('goldhill.bmp')
checkerboard('lenna.bmp')
checkerboard('pepper.bmp')
checkerboard('lenna2.png')
