# HOMEWORK1 - Author Ines Dzebic


## **Problem_4**


### **Task**

**Inverted Checkerboard**

The task was to create a program which will load all images from the images directory and to add a checkerboard pattern to the images. Instead of checkerboard having black and white 
squares, white squares should contain original pixel data, while the black squares should contain inverted pixel data.



### **Results**

Task was solved successfully. Since images, one by one, couldn't have been shown here, results can be found on the link below:

Result for checkerboard width of 64 pixels:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_4/Problem_4](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_4/Problem_4)



