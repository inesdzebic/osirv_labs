# HOMEWORK1 - Author Ines Dzebic

## **Problem_1**

### **Task**

The task was to create a program which will load all images from the images directory and to save the resulting images with filename of type: image_filename_V and image_filename_H, where: 

* image_filename_V will be image which has every other column set to 0, while

* image_filename_H will be image which has every other row set to 0.

## **Results**

Task was solved successfully. Since images, one by one, couldn't have been shown here, results can be found on the link below:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_1/Problem_1](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_1/Problem_1)