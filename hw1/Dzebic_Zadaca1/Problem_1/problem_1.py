import numpy as np
import cv2

#definiramo funkciju koja ce svaki drugi redak pocrniti
def redak(ime_slike):
    slika = cv2.imread('/home/osirv/labs/Dzebic/osirv_labs/images/'+ime_slike)
    redak = slika.shape[0]
    for x in range(0, redak):
        if x%2==0: #provjeravamo je li redak parni (tako vodimo racuna da je svaki drugi zacrnjen)
            slika[x] = 0 
    cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_1/Problem_1/"+ime_slike[0:-4]+"_H.png", slika)        
    #Za provjeru jesmo li sve dobro napravili uzimamo kao povratnu informaciju funkcije prikaz slike 
    povratna=cv2.imshow(''+ime_slike[0:-4]+''+'_H', slika)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return povratna

#definiramo funkciju koja ce svaki drugi stupac pocrniti
def stupac (ime_slike):
    slika = cv2.imread('/home/osirv/labs/Dzebic/osirv_labs/images/'+ime_slike)
    stupac = slika.shape[1]
    for y in range(0, stupac):
        if y%2==0: #provjeravamo je li redak parni (tako vodimo racuna da je svaki drugi zacrnjen)
            slika[:,y] = 0 
    cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_1/Problem_1/"+ime_slike[0:-4]+"_V.png", slika)  
    #Za provjeru jesmo li sve dobro napravili uzimamo kao povratnu informaciju funkciju prikaz slike
    povratna=cv2.imshow(''+ime_slike[0:-4]+''+'_V', slika)
    cv2.waitKey(0)
    cv2.destroyAllWindows()
    return povratna

#pozivamo funkcije
redak('airplane.bmp')
stupac('airplane.bmp')

redak('asbest.pgm')
stupac('asbest.pgm')

redak('baboon.bmp')
stupac('baboon.bmp')

redak('barbara.bmp')
stupac('barbara.bmp')

redak('barbara2.pgm')
stupac('barbara2.pgm')

redak('boats.bmp')
stupac('boats.bmp')

redak('BoatsColor.bmp')
stupac('BoatsColor.bmp')

redak('goldhill.bmp')
stupac('goldhill.bmp')

redak('lenna.bmp')
stupac('lenna.bmp')

redak('pepper.bmp')
stupac('pepper.bmp')

redak('lenna2.png')
stupac('lenna2.png')
