import numpy as np
import cv2
import math


def subsample(slika,f):
    redak = slika.shape[0]
    stupac = slika.shape[1]
    novi_redak=(int(redak/f))*f
    novi_stupac=(int(stupac/f))*f
    slika_crop=slika 
    slika_crop[0]=novi_redak
    slika_crop[1]=novi_stupac
    for i in range (0, slika_crop.shape[0]):
        for j in range (0, slika_crop.shape[1]):
            if((i % f == 0) and (j % f == 0)):
                avg = int(math.floor(np.average(slika_crop[i:i+f, j:j+f])))
                slika_crop[i:i+f, j:j+f] = avg
            
    return slika_crop

def zadatak6(ime_slike):
    for f in [8, 16, 32]:
        slika = cv2.imread('/home/osirv/labs/Dzebic/osirv_labs/images/'+ime_slike)
        if (len(slika.shape) == 2): 
            slika_subsampled = subsample(slika, f)
            cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_6/Problem_6_izrezano/"+ime_slike[0:-4]+"_sub"+str(f)+".png", slika)
           
        elif(len(slika.shape) == 3): 
            slika_blue = subsample(slika[:, :, 0], f)
            slika_green = subsample(slika[:, :, 1], f)
            slika_red = subsample(slika[:, :, 2], f)
            slika_subsampled = np.dstack((slika_blue, slika_green, slika_red))
            cv2.imwrite("/home/osirv/labs/Dzebic/osirv_labs/Zadaca/Problem_6/Problem_6/"+ime_slike[0:-4]+"_sub"+str(f)+".png", slika)
            
    

zadatak6('baboon.bmp')
zadatak6('pepper.bmp')

