# HOMEWORK1 - Author Ines Dzebic



## **Problem_6**


### **Task**

**Subsampling with averaging**
The task was to:
* implement the ` subsample() ` function, which takes as input a 2D numpy image
array ( grayscale image or single channel of a color image ) and a scaling
factor $` f `$. 

* run program on images `baboon` and `pepper` with subsampling factors
$` f \in [8, 16, 32] `$ and to save the resulting images with the following format: 
` image_filename_subN `, where the `N` is the subsampling factor $` f `$ used for 
that image.


Program should work on both, grayscale and color images. The function should return a subsampled image of the same dimensions as the original image. Even though subsampled image should be smaller than original image, we can simulate subsampling by setting all corresponding pixels of the resulting image to their average in the original image. 

### **Results**



Task was solved successfully. At first, it was solved by resizing original images, and after reading the task carefully again, it was solved as it should have been: "The function should return a subsampled image of the same dimensions as the original image."
Since images, one by one, couldn't have been shown here, results can be found on the link below:



Result - resizing:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_6/Problem_6_izrezano](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_6/Problem_6_izrezano)

Result - original size:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_6/Problem_6_original](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_6/Problem_6_original)







