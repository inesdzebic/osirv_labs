# HOMEWORK1 - Author Ines Dzebic


## **Problem_3**


### **Task**

**Checkerboard**

The task was to create a program which will load all images from the images directory and to add a checkerboard pattern to the images. Instead of checkerboard having black and white squares, checkerboard pattern should only have black squares, instead of white
squares, the original pixel data of the image should be left. 



### **Results**

Task was solved successfully. Since images, one by one, couldn't have been shown here, results can be found on the link below:

Result for checkerboard width of 64 pixels:
[https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_3/Problem_3](https://gitlab.com/inesdzebic/osirv_labs/tree/master/Zadaca/Problem_3/Problem_3)
